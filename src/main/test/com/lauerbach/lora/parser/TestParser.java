package com.lauerbach.lora.parser;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.apache.commons.codec.binary.Hex;
import org.junit.Test;

import com.lauerbach.ttn.mqttreceiver.payloadparser.Field;
import com.lauerbach.ttn.mqttreceiver.payloadparser.Parser;

public class TestParser {

	@Test
	public void test() throws Exception {
		Parser parser= new Parser();
		
		byte[] raw = Hex.decodeHex("874000580502A1");
		
		
		ArrayList<Field> r = parser.parse( raw);
		
		
		for (int i=0; i<r.size(); i++) {
			Field f = r.get(i);
			System.out.println( i+" "+f);
		}
		
	}

}
