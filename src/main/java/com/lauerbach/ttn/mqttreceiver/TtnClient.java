package com.lauerbach.ttn.mqttreceiver;

import org.apache.commons.codec.binary.Base64;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lauerbach.ttn.mqttreceiver.json.TtnUplinkMessage;

public class TtnClient implements MqttCallback {
	public final String REGION_EU="eu";
	
	Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
	public TtnClient( String region, String applicationId, String apiKey, TtnListener listener) throws MqttException {
		String mqttHost = "tcp://"+region+".thethings.network";
		MemoryPersistence persistence = new MemoryPersistence();
		MqttClient sampleClient = new MqttClient(mqttHost, "ttnClient-"+MqttClient.generateClientId(), persistence);
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setUserName( applicationId);
		connOpts.setPassword(apiKey.toCharArray());
		connOpts.setCleanSession(true);
		sampleClient.connect(connOpts);
		sampleClient.setCallback( new Main());
		sampleClient.subscribe("+/devices/#");
	}
	
	public void close() {
		
	}
	
	public void connectionLost(Throwable arg0) {
		arg0.printStackTrace();
	}

	public void deliveryComplete(IMqttDeliveryToken token) {
	}

	public void messageArrived(String tobic, MqttMessage message) throws Exception {
		System.out.println(message);
		
		TtnUplinkMessage obj = gson.fromJson( message.toString(), TtnUplinkMessage.class);
		
		Base64 decoder = new Base64();
		byte[] decodedBytes = decoder.decode( obj.getPayload_raw());
		System.out.println(new String(decodedBytes) + "\n") ;

	}

}
