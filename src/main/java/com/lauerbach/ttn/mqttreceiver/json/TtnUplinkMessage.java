package com.lauerbach.ttn.mqttreceiver.json;

import com.google.gson.annotations.Expose;

public class TtnUplinkMessage {
	@Expose private String app_id;
	@Expose private String dev_id;
	@Expose private String hardware_serial;
	@Expose private int port;
	@Expose private int counter;
	@Expose private String payload_raw;
	
	@Expose private Metadata metadata;
	
	public String getApp_id() {
		return app_id;
	}
	public void setApp_id(String app_id) {
		this.app_id = app_id;
	}
	public String getDev_id() {
		return dev_id;
	}
	public void setDev_id(String dev_id) {
		this.dev_id = dev_id;
	}
	public String getHardware_serial() {
		return hardware_serial;
	}
	public void setHardware_serial(String hardware_serial) {
		this.hardware_serial = hardware_serial;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public String getPayload_raw() {
		return payload_raw;
	}
	public void setPayload_raw(String payload_raw) {
		this.payload_raw = payload_raw;
	}
	public Metadata getMetadata() {
		return metadata;
	}
	public void setMetadata(Metadata metadata) {
		this.metadata = metadata;
	}

	
}
