package com.lauerbach.ttn.mqttreceiver.json;

import java.util.List;

import com.google.gson.annotations.Expose;

public class Metadata {
	@Expose private String time;
	@Expose private double frequency;
	@Expose private String modulation;
	@Expose private String data_rate;
	@Expose private long   airtime;
	@Expose private String coding_rate;
	
	@Expose private List<Gateway> gateways;

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	public String getModulation() {
		return modulation;
	}

	public void setModulation(String modulation) {
		this.modulation = modulation;
	}

	public String getData_rate() {
		return data_rate;
	}

	public void setData_rate(String data_rate) {
		this.data_rate = data_rate;
	}

	public long getAirtime() {
		return airtime;
	}

	public void setAirtime(long airtime) {
		this.airtime = airtime;
	}

	public String getCoding_rate() {
		return coding_rate;
	}

	public void setCoding_rate(String coding_rate) {
		this.coding_rate = coding_rate;
	}

	public List<Gateway> getGateways() {
		return gateways;
	}

	public void setGateways(List<Gateway> gateways) {
		this.gateways = gateways;
	}
	
}
