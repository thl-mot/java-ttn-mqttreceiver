package com.lauerbach.ttn.mqttreceiver.json;

import com.google.gson.annotations.Expose;

public class Gateway {
	@Expose private String gtw_id;
	@Expose private String timestamp;
	@Expose private String time;
	@Expose private int channel;
	@Expose private int rssi;
	@Expose private double snr;
	@Expose private int rf_chain;
	@Expose private double latitude;
	@Expose private double longitude;
	
	public String getGtw_id() {
		return gtw_id;
	}
	public void setGtw_id(String gtw_id) {
		this.gtw_id = gtw_id;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public int getChannel() {
		return channel;
	}
	public void setChannel(int channel) {
		this.channel = channel;
	}
	public int getRssi() {
		return rssi;
	}
	public void setRssi(int rssi) {
		this.rssi = rssi;
	}
	public double getSnr() {
		return snr;
	}
	public void setSnr(double snr) {
		this.snr = snr;
	}
	public int getRf_chain() {
		return rf_chain;
	}
	public void setRf_chain(int rf_chain) {
		this.rf_chain = rf_chain;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	
}
