package com.lauerbach.ttn.mqttreceiver;

import com.lauerbach.ttn.mqttreceiver.json.TtnUplinkMessage;

public interface TtnListener {
	void messageReceived( char[] data, TtnUplinkMessage message);
}
