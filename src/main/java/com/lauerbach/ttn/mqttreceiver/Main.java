package com.lauerbach.ttn.mqttreceiver;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.lauerbach.ttn.mqttreceiver.json.Gateway;
import com.lauerbach.ttn.mqttreceiver.json.TtnUplinkMessage;
import com.lauerbach.ttn.mqttreceiver.payloadparser.Field;
import com.lauerbach.ttn.mqttreceiver.payloadparser.Parser;

public class Main implements MqttCallback {

	Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
			.create();

	Parser parser = new Parser();

	public static void main(String[] args) {
		MemoryPersistence persistence = new MemoryPersistence();
		try {
			Properties properties = new Properties();
			FileInputStream is = new FileInputStream("credentials.properties");
			properties.load(is);
			is.close();

			String mqttHost = "tcp://eu.thethings.network";
			String mqttUsername = properties.getProperty("mqtt-username");
			String mqttPassword = properties.getProperty("mqtt-password");

			MqttClient sampleClient = new MqttClient(mqttHost, "testclient",
					persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setUserName(mqttUsername);
			connOpts.setPassword(mqttPassword.toCharArray());
			connOpts.setCleanSession(true);
			sampleClient.connect(connOpts);
			sampleClient.setCallback(new Main());
			sampleClient.subscribe("+/devices/#");
			System.out.println("Connected");
			// sampleClient.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void connectionLost(Throwable arg0) {
	}

	public void deliveryComplete(IMqttDeliveryToken arg0) {
	}

	public void messageArrived(String topic, MqttMessage message)
			throws Exception {
		try {
			System.out.println(message);

			TtnUplinkMessage obj = gson.fromJson(message.toString(),
					TtnUplinkMessage.class);

			System.out.println("APP:     " + obj.getApp_id());
			System.out.println("Dev:     " + obj.getDev_id());

			if (topic.endsWith("/up")) {
				Base64 decoder = new Base64();
				byte[] decodedBytes = decoder.decode(obj.getPayload_raw());
				System.out.println(new String(decodedBytes) + "\n");

				System.out.println("Payload: "
						+ Hex.encodeHexString(decodedBytes));
				System.out.println("Counter: " + obj.getCounter());

				System.out.println("Fields");
				ArrayList<Field> data = parser.parse(decodedBytes);
				for (int i = 0; i < data.size(); i++) {
					Field f = data.get(i);
					System.out.println("  " + i + " " + f.toString());
				}
				System.out.println("");
			}

			Iterator<Gateway> i = obj.getMetadata().getGateways().iterator();
			while (i.hasNext()) {
				Gateway g = i.next();
				System.out.println("  " + g.getGtw_id() + "   RSSI:"
						+ g.getRssi() + "  ");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
