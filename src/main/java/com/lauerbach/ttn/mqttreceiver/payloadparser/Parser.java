package com.lauerbach.ttn.mqttreceiver.payloadparser;

import java.util.ArrayList;
import java.util.HashMap;

public class Parser {

	public static final HashMap<Integer, String> typeMap = new HashMap<Integer, String>();

	public Double getDouble( byte b1, byte b2, int div) {
		return new Double((Math.abs( b1)*256+Math.abs( b2)/div));
	}

	public Double getDouble( byte b1, int div) {
		return new Double(Math.abs( b1)/div);
	}

	public Integer getInt( byte b1, byte b2) {
		return Math.abs( b1)*256+Math.abs( b2);
	}
	
	public Integer getInt( byte b1) {
		return Math.abs( b1);
	}
	
	public ArrayList<Field> parse(byte[] data) throws PayloadParserException {
		ArrayList<Field> result = new ArrayList<Field>();
		if ((data[0] & 0x80) == 0) {
			throw new PayloadParserException("Header error.");
		}
		int size = data[0] & 0x7F;

		if (size != data.length) {
			throw new PayloadParserException(
					"Invalid data size: Header demands " + size + " but found "
							+ data.length);
		}

		int i = 1;
		while (i < data.length) {
			int type = data[i];
			switch (type) {
			case 0x01:
				result.add( new Field( type, new Double((data[i+1] - 100) + data[i+2]/ 100)));
				i = i + 3;
				break;
			case 0x02:
				result.add( new Field( type, getInt( data[i+1])));
				i = i + 2;
				break;
			case 0x03:
				result.add( new Field( type, getInt( data[i+1])));
				i=i+3;
				break;
			case 0x04:
				result.add( new Field( type, getInt( data[i+1])));
				i=i+2;
				break;
			case 0x05:
				result.add( new Field( type, getDouble( data[i+1],data[i+2],10)));
				i=i+3;
				break;
			case 0x40:
				result.add( new Field( type, getDouble( data[i+1], data[i+2], 10)));
				i=i+3;
				break;
			default:
				throw new PayloadParserException("Unknown data type " + type);
			}
		}

		return result;
	}

	static {
		typeMap.put(0x01, "Temperature (°C)");
		typeMap.put(0x02, "Humidity (%)");
		typeMap.put(0x03, "Airpressure");
		typeMap.put(0x04, "Moisture (%)");
		typeMap.put(0x05, "Moisture (%)");
		typeMap.put(0x06, "Distance (mm)");
		typeMap.put(0x40, "Battery (V)");
	}
}
