package com.lauerbach.ttn.mqttreceiver.payloadparser;

public class PayloadParserException extends Exception {

	public PayloadParserException(String string) {
		super( string);
	}

}
