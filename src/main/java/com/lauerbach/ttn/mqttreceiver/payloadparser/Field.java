package com.lauerbach.ttn.mqttreceiver.payloadparser;

import java.util.HashMap;

public class Field {
	
	int type;
	Object value;
	
	public Field(int type, Object value) {
		super();
		this.type = type;
		this.value = value;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return Parser.typeMap.get(type)+" Value="+value;
	}
	
}
